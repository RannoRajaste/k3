import java.util.Arrays;
import java.util.LinkedList;

public class DoubleStack implements Cloneable {

    private LinkedList<Double> stack;
    private static String[] operations = new String[]{"-", "+", "*", "/", "ROT", "SWAP"};

    DoubleStack() {
        stack = new LinkedList<>();
    }

    @Override
    public DoubleStack clone() throws CloneNotSupportedException {
        DoubleStack cloned = (DoubleStack) super.clone();
        cloned.stack = new LinkedList<>(stack);
        return cloned;
    }

   public static void main(String[] args) {
       System.out.println(interpret("2 5 SWAP -")); // 3
       System.out.println(interpret("2 5 9 ROT - +")); // 12
       System.out.println(interpret("2 5 9 ROT + SWAP -")); // 6
       System.out.println(interpret("9 ROT"));
       System.out.println(interpret("9 SWAP"));
       System.out.println(interpret("9 2 ROT"));
       System.out.println(interpret("3 2 1 + "));
       System.out.println(interpret("3 2 + -"));
       System.out.println(interpret(""));
       System.out.println(interpret("1 + -"));

   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (double a) {
      stack.add(a);
   }

   public double pop() {
      if (stEmpty()){
         throw new StackUnderFlowException("Cannot pop, stack is empty!");
      }
      return stack.remove(getTosIndex());
   }

   public void op (String s) {

      if (!Arrays.asList(operations).contains(s)) {
         throw new IllegalArgumentException();
      }

      if (getTosIndex() < 1) {
         throw new StackUnderFlowException();
      }

      Double secondOperand = pop();
      Double firstOperand = pop();

      if ("+".equals(s)) {
         push(firstOperand + secondOperand);
      } else if ("-".equals(s)) {
         push(firstOperand - secondOperand);
      } else if ("*".equals(s)) {
         push(firstOperand * secondOperand);
      } else if ("/".equals(s)) {
         push(firstOperand / secondOperand);
      } else if ("SWAP".equals(s.toUpperCase())) {
         push(secondOperand);
         push(firstOperand);
      } else if ("ROT".equals(s.toUpperCase())) {
         if (!stEmpty()) {
            Double thirdOperand = pop();
            push(firstOperand);
            push(secondOperand);
            push(thirdOperand);
         } else {
            throw new StackUnderFlowException();
         }
      }
   }
   public double tos() {
      if (stEmpty()) {
         throw new StackUnderFlowException("Stack is empty!");
      }
      return stack.get(getTosIndex());
   }

   @Override
   public boolean equals (Object o) {
      if (o instanceof DoubleStack){
         return ((DoubleStack) o).stack.equals(this.stack);
      }
      return false;
   }


   @Override
   public String toString() {
      return stack.toString();
   }

   public static double interpret (String pol) {

       int numberOfArgumentsNeeded = 2;

      if (pol == null || pol.trim().equals("")) {
         throw new MalformedExpressionException("No expression inserted!");
      }

      String errorMsg = String.format("Expression \"%s\" is incorrect.", pol);

      pol = pol.trim();
      String[] expressionElements = pol.split("\\s+");
      DoubleStack stack = new DoubleStack();

      for (int i = 0; i < expressionElements.length; i++) {
         String element = expressionElements[i];

         if (isValue(element)) {
            stack.push(Double.parseDouble(element));

         } else {
            if (i == expressionElements.length - 1 && stack.getTosIndex() > 1 && !element.equals("ROT")) {
               throw new MalformedExpressionException(String.format("%s Only 2 arguments allowed for operation \"%s\", found %s!",
                       errorMsg, element, stack.getTosIndex() + 1));
            }
            try {

               if (element.equals("ROT")){
                  numberOfArgumentsNeeded = 3;
               }

               stack.op(element);

            } catch (StackUnderFlowException e) {
               throw new MalformedExpressionException(String.format("%s Need %s arguments for operation \"%s\"!",
                       errorMsg, numberOfArgumentsNeeded, element));

            } catch (IllegalArgumentException e) {
               throw new MalformedExpressionException(String.format("%s \"%s\" is neither a number nor a valid arithmetic operation!",
                       errorMsg, element));
            }
         }
      }
      if (stack.getTosIndex() > 0) {
         throw new MalformedExpressionException(String.format("%s Too many numbers: no arithmetic operation given for arguments %s",
                 errorMsg, stack.toString()));
      }
      return stack.pop();

    }

   private static boolean isValue(String input){
      try {
         Double.parseDouble(input);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   private int getTosIndex(){
      return stack.size() - 1;
   }
}

class StackUnderFlowException extends RuntimeException {

   StackUnderFlowException(String msg){
      super(msg);
   }

   StackUnderFlowException(){
      super();
   }
}

class MalformedExpressionException extends RuntimeException {
   MalformedExpressionException(String msg){
      super(msg);
   }
}